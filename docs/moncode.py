import markdown
from pathlib import Path

# Define the Markdown file path
file_path = Path('index.md')

# Read the contents of the Markdown file
with open(file_path, 'r') as file:
    content = file.read()

# Convert the Markdown content to HTML and parse the metadata
md = markdown.Markdown(extensions=['markdown.extensions.meta'])
html = md.convert(content)
metadata = md.Meta

# Print the metadata
print(metadata)