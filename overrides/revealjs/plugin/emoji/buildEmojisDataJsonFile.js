import emojisData from "./rawEmojisData.js";
import fs from "fs";

// Raw Emoji Data from "https://raw.githubusercontent.com/iamcal/emoji-data/master/emoji.json"

const buildEmojisUrlObject = (emojisData) => {
    let emojisObject = {};
    const skinShortName = {
        "1F3B": "light", 
        "1F3C": "medium_light", 
        "1F3D": "medium", 
        "1F3E": "medium_dark", 
        "1F3F": "dark" 
    }
    const baseUrl = "https://cdn.jsdelivr.net/gh/twitter/twemoji@14.0.2/assets/svg/";
    emojisData.forEach( emoji => {
        // emojisObject[emoji.short_name] = baseUrl + cleanUrl(emoji.image);
        emojisObject[emoji.short_name] = {
            "altText": getEmojiGrapheme(emoji.unified),
            "url": baseUrl + cleanUrl(emoji.image)
        };

        if (emoji.skin_variations) { // if skin tones exist for this emoji, add them
            // console.log("SKINS DETECTED");
            Array.from(Object.keys(emoji.skin_variations)).forEach( (skin,i)  => {
                // console.log(emoji.skin_variations[`${skin}`].image);
                emojisObject[emoji.short_name+"_"+skinShortName[Object.keys(skinShortName)[i]]] = {
                    "altText": getEmojiGrapheme(emoji.skin_variations[`${skin}`].unified),
                    "url": baseUrl + cleanUrl(emoji.skin_variations[`${skin}`].image)
                };
            });
        }
        emoji.short_names.forEach( shortname => { // Add any shortname aliases
                emojisObject[shortname] = {
                    "altText": getEmojiGrapheme(emoji.unified),
                    "url": baseUrl + cleanUrl(emoji.image)
                };
            });
    });
    return emojisObject
}

const getEmojiGrapheme = (codePointString) => {
    return codePointString.split('-')
                    .map((codePoint) => (
                        String.fromCodePoint(`0x${codePoint}`)
                        ))
                    .join('');                     
}

// const getParsedUnicodeFrom = (s) => {
//     let uni='';
//     s.split("-").forEach(uniSequence => {
//         uni = uni + String.fromCharCode("0x"+`${uniSequence}`);
//         // uni = uni + String.fromCharCode(parseInt(`${uniSequence}`, 16));
//     });
// }

// function codePoint2EmojiSymbol(s) {
//     const surrogates = getSurrogatePair(parseInt("0x"+s, 16)); // => [0xD83D, 0xDE00]
//     const hexSurrogates = surrogates.map( surrogate => surrogate.toString(16))
//     var symbol = "";
//     hexSurrogates.forEach( hexSurrogate => {
//         console.log("hexSurrogate=", hexSurrogate);
//         symbol = symbol + String.fromCharCode("0x"+hexSurrogate);
//     }) 
//     return symbol
// }

// // var res = codePoint2EmojiSymbol("1F9D1-200D-1F3A8");
// var res = codePoint2EmojiSymbol("1F9D1");
// const codePointString = '1F9D1-1F3FB-200D-1F3A8';

// function getSurrogatePair(astralCodePoint) {
//     let highSurrogate = 
//        Math.floor((astralCodePoint - 0x10000) / 0x400) + 0xD800;
//     let lowSurrogate = (astralCodePoint - 0x10000) % 0x400 + 0xDC00;
//     return [highSurrogate, lowSurrogate];
//   }

// function getAstralCodePoint(highSurrogate, lowSurrogate) {
// return (highSurrogate - 0xD800) * 0x400 
//     + lowSurrogate - 0xDC00 + 0x10000;
// }

// getAstralCodePoint(0xD83D, 0xDE00); // => 0x1F600


const cleanUrl = (image) => { // fix 'xxxx-fe0f.svg' image name to 'xxxx.svg'
    const nbOccurences = (image.match(/-/g) || []).length;
    const n = image.length;
    if (nbOccurences == 1 && n >= 9 && image.substring(n-9,n) == "-fe0f.svg") {
        image = image.substring(0,n-9) + ".svg";
    }
    return image
}

const emojis = buildEmojisUrlObject(emojisData);
console.log("emojis=", emojis);

const emojisString = JSON.stringify(emojis);
fs.writeFile("emojisData.json", emojisString, (err, result) => {
    if(err) { 
        console.log('error', err);
    }
});
